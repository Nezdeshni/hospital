const express = require('express')
const mysql = require("mysql")
const morgan = require('morgan')
var db = require('../db');
const router = express.Router()

router.use(morgan("tiny"))

router.post("/create", (req, res) => {
    const fname = req.body.fname;
    const lname = req.body.lname;

    const email = req.body.email;
    const username = req.body.username;
    const password = req.body.password;
    const role="customer"
    db.query(
      "INSERT INTO customer (fname, lname,email, username, password,role) VALUES (?,?,?,?,?,?)",
      [fname, lname, email, username, password,role],
      (err, result) => {
        if (err) {
          console.log(err);
        } else {
          res.send("Values Inserted");
        }
      }
    );
  });

module.exports = router
