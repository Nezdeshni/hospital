const express = require('express')
const session = require('express-session');
const mysql = require("mysql")
const morgan = require('morgan')
const db = require('../db');
const {stringify} = require("nodemon/lib/utils");
const router = express.Router()
const fs = require('fs');
const htmlTo = require('html2xlsx');


router.post("/stock/naorders", (req,response)=>{
    let sql = `SELECT orders.sqn, orders.printform_data, orders.order_state, orders.sub_totall, customer.lname, customer.fname, customer.email FROM orders,customer WHERE customer.cid=orders.CID AND orders.order_state="created";`
    db.query(sql,[],(err, result)=>{
        if(err){console.log(err)}
        else
        {response.status(200).send(result)}
    })


})
router.post("/stock/myorders", (req,response)=>{
    console.log(req);
    let CID=req.body.me;

    let sql ="SELECT * from orders inner join customer on customer.cid=orders.CID AND orders.CID="+CID+";"
    db.query(sql,[],(err, result)=>{
        if(err){console.log(err);response.status(400).send(err)}
        else
        {response.status(200).send(result)}
    })


})
router.post("/stock/attentionorders", (req,response)=>{
    console.log(req);


    let sql = `SELECT * FROM orders WHERE orders.order_state="created";`
    db.query(sql,[],(err, result)=>{
        if(err){console.log(err);response.status(400).send(err)}
        else
        {response.status(200).send(result.length)}
    })


})

router.post("/stock/orderrevoke", (req,response)=>{
    let num=req.body.orn;
    let  sql=`DELETE FROM orders WHERE sqn=?`
    db.query(sql,[num],(err, result)=>{
        if(err){response.status(400).send(err)}
        else
        {response.status(200).send("removed")}
    })


})
router.post("/stock/orderapprove", (req,response)=>{
    let num=req.body.orn;
    let  sql=`UPDATE orders SET orders.order_state='ontheway' WHERE orders.sqn=?`
    db.query(sql,[num],(err, result)=>{
        if(err){response.status(400).send(err)}
        else
        {response.status(200).send("approved")}
    })


})


const XLSX = require('xlsx');

// Connect to the database
// Fetch orders and related data
const fetchOrders = async () => {
    return new Promise((resolve, reject) => {
        const query = `          
          SELECT o.sqn,c.fname, c.lname, od.datetime, o.sub_totall, o.printform_data,c.username
        FROM orders o
        JOIN customer c ON o.CID = c.cid
        JOIN orders od ON o.sqn = od.sqn
     ORDER BY sqn desc LIMIT 1
        `;
        try {
            db.query(query, (err, res) => {
                if (err) {
                    reject(err);
                }
                resolve(res[0]);
            });
        } catch (err) {
            console.error('Error executing query', err.stack);
            reject(err);
        }
    });
};
const calculateColumnWidths = (data) => {
    const colWidths = data[0].map((_, colIndex) => {
        return Math.max(...data.map(row => (row[colIndex] ? row[colIndex].toString().length : 0)));
    });
    return colWidths.map(width => ({ width: Math.max(width, 10) }));
};
// Generate XLSX file
const generateXLSX = async () => {
    const order = await fetchOrders();

    if (!order) {
        console.log('No orders found');
        return;
    }

    // Organization details (assuming static for this example)
    const organizationName = "Медизделья ИП Павлов";
    const organizationAddress = "МУ Мухино, пос. Озёрное, улица Герасима стр. 18/52";

    // Customer details
    const customerName = `${order.lname}`;
    const customerAddress = `${order.fname}`;
    const orderDate = order.datetime;
    const subtotal = order.sub_totall;
    // const dealer = order.username;

    // Prepare data for XLSX
    const printformData = JSON.parse(order.printform_data);
    const data = [
        [],
        [`Поставка для контрагента: ${customerName}`],
        [`По адресу:`],
        [`${customerAddress}`],
        [`Order Date: ${new Date(orderDate).toLocaleDateString()}`],
        [],
        ['Номер', 'Количество', 'Производитель', 'Наименование', 'Цена'],
    ];

    Object.values(printformData).forEach(item => {
        data.push([
            "SNTP_"+item.sr_no.toString().padStart(8,"0"),
            item.q,
            item.mfr,
            item.n,
            item.cpi
        ]);
    });

    data.push([]);
    data.push(["","","","",`Итого: ${subtotal}`]);
    data.push([`ПОСТАВЩИК`]);
    data.push([`юр. лицо: ${organizationName}`]);
    data.push([`компания зарегистрирована по адресу:`]);
    data.push([`${organizationAddress}`]);


    // Create a new workbook and sheet
    const wb = XLSX.utils.book_new();
    const ws = XLSX.utils.aoa_to_sheet(data);
    // Calculate and set column widths

    let objectMaxLength = []

    data.map(arr => {
        Object.keys(arr).map(key => {
            let value = arr[key] === null ? '' : arr[key]

            if (typeof value === 'number')
            {
                return objectMaxLength[key] = 10
            }

            objectMaxLength[key] = objectMaxLength[key] >= value.length ? objectMaxLength[key]  : value.length
        })
    })

    let worksheetCols = objectMaxLength.map(width => {
        return {
            width
        }
    })

    ws["!cols"] = worksheetCols;
    XLSX.utils.book_append_sheet(wb, ws, 'Invoice');

    // Write XLSX file
    XLSX.writeFile(wb, `./../client/public/invoices/invoices_${order.sqn.toString().padStart(8,"0")}_.xlsx`);
    console.log('Invoices generated successfully');
};




router.post("/stock/insertorder", (req,response)=>{
    console.log(req.session.ud)
    console.log(req.body)
        //response.send({ud:req.session,b:req.body})
    let o=JSON.parse(req.body.fd).ord;
    let s=JSON.parse(req.body.fd).ses;
    let um=JSON.parse(req.body.fd).um;
   // um=um.map(e=>{return [{"qty_left":e[1]},{"sr_no":e[0]}]})
   // let l=[um.map(e=>e[1]),um.map(e=>e[0])]
    console.log(um)
    let sub=Object.values(o).reduce((p,c)=>{return p+c.cpi*c.q},0)
    let sql = "insert INTO orders (printform_data,CID,sub_totall,order_state) VALUES (?,?,?,?)";
    var trsql=`INSERT INTO transaction (billno, totalcost, C_ID) VALUES ('@','${sub}','${s.data.cid}')`
    //um=um.map(e=>{return {"qty_left":e[1],"sr_no":e[0]}})
    db.query(sql,[JSON.stringify(o),s.data.cid,sub,"created"],(err, result)=>{
        if(err){
            console.log(err)
        }
        else
        {
            db.query("SELECT * FROM `orders` ORDER BY sqn desc LIMIT 1",{},(eee,rrr)=>{
               let bn= rrr[0].sqn;
                let ddate=rrr[0].datetime;
                let trsq=trsql.replace("@",bn.toString());
                db.query(trsq,{},console.log)
            })
            generateXLSX().then(() => response.status(200).send("ordered"));


            let sql=um.map(e=>"UPDATE med SET qty_left="+e[1]+ " WHERE sr_no="+e[0]+";")
            sql.map(e=>db.query(e,[],(err)=>{if(err){console.log(err)}}))
        }
    })


})
//add new medicin in stock
router.post("/stock/insert", (req,response)=>{
    const med_name = req.body.med_name
    const qty_left = req.body.qty_left
    const med_cost = req.body.med_cost
    const exp_date = req.body.exp_date
    const med_mfg = req.body.med_mfg
    const rac_loc = req.body.rac_loc
    const mfg_date = req.body.mfg_date
    let sql = 'INSERT INTO med (med_name, qty_left, med_cost, exp_date, med_mfg, rac_loc, mfg_date) VALUES (?,?,?,?,?,?,?)'

    db.query(sql, [med_name, qty_left,  med_cost,  exp_date, med_mfg, rac_loc, mfg_date],(err, result)=>{
        if(err){
            console.log(err)
        }
        else
        {
            response.send("log inserted")
        }
    })
})

router.put("/stock/update", (req, response)=>{
     const sr_no = req.body.sr_no;
    const med_name = req.body.med_name
    const qty_left = req.body.qty_left
    const med_cost = req.body.med_cost
    const exp_date = req.body.exp_date
    const med_mfg = req.body.med_mfg
    const rac_loc = req.body.rac_loc
    const mfg_date = req.body.mfg_date
   console.log(med_name)
   console.log(sr_no)
    if(!sr_no || !med_name)
    {
      return response.status(400).send("Sometdsdhing went wrong");
    }
    var check_sql = "SELECT EXISTS(SELECT * FROM med WHERE sr_no = ?)"
    const check = db.query(check_sql, [sr_no]);
    var sql = "UPDATE med SET med_name = ?, qty_left = ?, med_cost = ?, exp_date = ?, med_mfg = ?, rac_loc=?, mfg_date = ? WHERE sr_no = ?";
    // if(check){
    db.query(sql,
        [ med_name, qty_left, med_cost, exp_date, med_mfg, rac_loc, mfg_date, sr_no],  (err, result)=> {
        if (err) {
            console.log(err);
            res.status(400).send("Encountered error, contact admin.");
            return
        }
        else {
            if(!result.affectedRows){
              response.status(404).send("Not found!");
            }
            else
              response.send("Updated!");
           return
          }
      });
    // }
})




//delete medicine
router.post("/stock/delete",(req,res) => {

    const sr_no = req.body.sr_no;
    if(!sr_no)
    {
      res.status(400).send("Wrong Serial number");
    }

    db.query(
      "DELETE FROM med where sr_no=?",
      [sr_no],
      (err, result) => {
        if (err) {
          console.log(err);
          res.status(400).send("Encountered error, contact admin.");
        }
        else {
          if(!result.affectedRows)
            res.send("Not found!");
          else
            res.send("Deleted!");
        }
      }
    );
  });


router.post("/stock/search", (req, res) => {

  const sr_no = req.body.sr_no;
  if(!sr_no)
  {
    console.log(sr_no)
    res.status(400).send("Wrong medicine ID");
    return;
  }

  const sql_query="SELECT * FROM med where sr_no=?";

  db.query(
    sql_query,
    [sr_no],
    (err, result) => {
      if (err) {
        res.status(400).send("Encountered error, contact admin.");
      }
      else {
        console.log(result)
        console.log(res[0])
        if(result.length==0){
          console.log(result[0])
          console.log("cha muda")
         return;
        }
        else {
          console.log("hi")
          res.send(result);
          return;}
      }
    }
  );
});


router.get("/stock/expirySort", (req, res) => {

  db.query("SELECT * FROM med order by exp_date", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});


router.get("/stock/qtySort", (req, res) => {
  db.query("SELECT * from qty_sort", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

router.get("/stock/shelfLife", (req, res) => {
  db.query("SELECT sr_no, med_name, shelf_life_year(mfg_date, exp_date) as shelf_life_year, shelf_life_month(mfg_date, exp_date) as shelf_life_month from med", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});



module.exports = router
