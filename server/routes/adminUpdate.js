const express = require('express')
const mysql = require("mysql")
const morgan = require('morgan')
var db = require('../db');
const router = express.Router()

router.use(morgan("tiny"))


async function getRole(u){
    return new Promise((resolve,reject)=>{
    db.query(
        "SELECT role FROM customer WHERE username = ?",[u],(err,result)=>{
            if(err){reject(err)}
            resolve(result[0].role)
        })
    })
}

async function isVerifyed(u,p){
    return new Promise((resolve,reject)=>{
        db.query(
            "SELECT fname, lname, username, email , cid FROM customer WHERE username = ? AND password= ?",
            [u,p],
            (err, result) => {
                if(err){reject(err)}
                if(!result[0])
                {
                    console.log(result);
                    console.log("Invalid data 32")
                    return;
                    resolve(false)
                } else{
                resolve(true)
                    }
            })
    })
}


router.post("/userVerifyed",async (req, res) => {
let v=await isVerifyed( req.body.username,req.body.password);
if(v){
    db.query(
        "SELECT fname, lname, username, email , cid FROM customer WHERE username = ?",
        [username,password],
        (err, result) => {
            if(err){
                res.status(400).send({"username":0})
            }
            res.status(200).send(result)
        })
}
else{
    res.status(400).send({"varify":0})

}
})

router.post("/adminUpdate",async (req, res) => {
  const fname = req.body.fname;
  const lname = req.body.lname;
  const age = req.body.age;
  const pincode = req.body.pincode;
  const email = req.body.email;
  const username = req.body.username;
  const password = req.body.password;

  console.log(JSON.stringify(req.body, null, 4));

  if(!username || !password)
  {
    res.status(400).send({'password':0});
    console.log("Incomplete form submission")
    return;
  }
  if(await getRole(username)==="admin"){


  db.query(
    "SELECT fname, lname, username, email , cid FROM customer WHERE username = ?",
    [username],
    (err, result) => {
      if (err) {
        res.status(400).send({'password':0});
        console.log("Invalid data 2");
      } else {
        if(!result[0])
        {
          res.status(400).send({'password':0});
          console.log(result);
          console.log("Invalid data 32")
          return;
        }
        else
        {
          console.log("Correct data provided");
          res.status(200).send(result);
          console.log(result)
          return;
        }
      }
    }
  );
  }
    else{

      db.query(
          "SELECT fname, lname, username, email , cid FROM customer WHERE username = ? AND password= ?",
          [username,password],
          (err, result) => {
              if (err) {
                  res.status(400).send({'password':0});
                  console.log("Invalid data 2");
              } else {
                  if(!result[0])
                  {
                      res.status(400).send({'password':0});
                      console.log(result);
                      console.log("Invalid data 32")
                      return;
                  }
                  else
                  {
                      console.log("Correct data provided");
                      res.status(200).send(result);
                      console.log(result)
                      return;
                  }
              }
          }
      );

  }
});

router.post("/adminReset", (req, res) => {
        const fname = req.body.fname;
        const lname = req.body.lname;
        const npassword = req.body.npassword;
        const email = req.body.email;
        const username = req.body.username;
        const cid = req.body.cid;
    if(!fname || !lname ||  !email)
    {
      res.status(400).send({'reset':0});
      console.log("Incomplete data entered")
      return;
    }
    db.query(
      "UPDATE customer SET fname = ?, lname = ?, email = ?, username=?, password=? Where cid = ?",
      [fname, lname,email,username, npassword, cid],
      (err, result) => {
        if (err) {
            console.log(err)
          res.status(400).send({'reset':0});
          console.log("Invalid data 2");
        } else {


            console.log("Account updation successful");
            res.status(200).send({'reset':1});
            console.log(result)
            return;

        }
      }
    );
  });


router.post("/userReset",async (req, res) => {
    const fname = req.body.fname;
    const lname = req.body.lname;
    const npassword = req.body.npassword;
    const email = req.body.email;
    const username = req.body.username;
    const cid = req.body.cid;
    if(!fname || !lname ||  !email)
    {
        res.status(400).send({'reset':0});
        console.log("Incomplete data entered")
        return;
    }
    if(await isVerifyed(username,npassword)){
    db.query(
        "UPDATE customer SET fname = ?, lname = ?, email = ?, username=?, password=? Where cid = ?",
        [fname, lname,email,username, npassword, cid],
        (err, result) => {
            if (err) {
                console.log(err)
                res.status(400).send({'reset':0});
                console.log("Invalid data 2");
            } else {


                console.log("Account updation successful");
                res.status(200).send({'reset':1});
                console.log(result)
                return;

            }
        }
    );}
    else{

    }
});

module.exports = router
