const express = require('express')
const session = require('express-session');
const mysql = require("mysql")
const morgan = require('morgan')
var db = require('../db');
const router = express.Router()

router.use(morgan("tiny"))

router.post("/authenticate", (req, res) => {
  const username = req.body.username;
  console.log(JSON.stringify(req.body, null, 4));
  const password = req.body.password;

  if(!username || !password)
  {
    res.status(400).send({'login':0});
    console.log("Login failed 1")
    return;
  }

  db.query(
    "SELECT username,cid,lname,fname, password, role FROM customer WHERE username = ? AND password = ?",
    [username, password],
    (err, result) => {
      console.log(result)
      if (err) {
        res.status(400).send({'login':0});
        console.log("Login failed 2");
      } else {
        if(!result[0])
        {
          res.status(400).send({'login':0});
          console.log(result);
          console.log("Login failed 3")
          return;
        }
        else
        {
          console.log("Login successful");
          req.session.ud={data:result[0]}
              // req.session.save();
          console.log(result)
          res.status(200).send({'data':result[0],'login':1});

          return;
        }
      }
    }
  );
});

module.exports = router
