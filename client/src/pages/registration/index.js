import {useState} from 'react';
// import axios from 'axios';
import {Headline ,HeadH1, HeadH2} from './registerStyle/title'
import {Video, Container, ImgWrap, Img, FormWrap, FormContent, Form, FormInput, FormH1, FormLabel, FormButton} from './registerStyle/register'
import logo from './images/s2.svg'
import loc from './images/location.svg'
import pharma from './images/pharma.png'
import medicine from './images/medicine.png'
import {Footer, Heading, Contact, Location, Image} from './registerStyle/FooterElements'

const Registration = () => {

  const [lname, setLastname] = useState("");
  const [fname, setFirstname] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");


  const addUser = () =>{
    console.log(username);
    fetch("http://localhost:1300/register/create",{
            method:"post",
            headers:{
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
              fname: fname,
              lname:lname,
              email:email,
              username: username,
              password: password,
            })
        })
        // .then(res=> res.json())
        .then(data=>{
          console.log(data);
          console.log("success");
          window.location.href='/signin';
          alert("Регистрация прошла успешно! \n Войти с помощью созданного идентификатора.");
        })
        .catch(err=>{
            console.log("we have an error in catch",err);
        })
      }

  // const addUser = () =>{
  //   console.log(username);
  //   axios.post('http://localhost:1300/create', {
  //     fname: fname,
  //     lname:lname,
  //     age:age,
  //     pincode:pincode,
  //     email:email,
  //     username: username,
  //     password: passwordHash.generate(password),
  //   }).then(() => {
  //     console.log("success");
  //     window.location.href='/';
  //     alert("Registration successful! \n Login using the ID created.");
  // //   });
  // // };

  return (
    <>
     <Headline>
      <HeadH1> СУЗ Медизделия </HeadH1>
     </Headline>

<Container>
     <FormWrap>
        <FormContent>
          <Form onsubmit = "redirectLog(e)">
            <FormH1>Присоединиться</FormH1>
            <FormLabel>Адрес организации</FormLabel>
              <FormInput type = 'text'  onChange ={(event) => {setFirstname(event.target.value)}}/>
              <FormLabel>Наименование организации</FormLabel>
              <FormInput type = 'text' required onChange ={(event) => {setLastname(event.target.value)}}/>
              <FormLabel>Email организации</FormLabel>
              <FormInput type = 'email' required onChange ={(event) => {setEmail(event.target.value)}}/>
            <FormLabel>Учётная запись</FormLabel>
              <FormInput type = 'text' required onChange ={(event) => {setUsername(event.target.value)}}/>
              <FormLabel>Пароль</FormLabel>
              <FormInput type = 'password' required onChange ={(event) => {setPassword(event.target.value)}}/>
            <FormButton onClick = {addUser}>Продолжить</FormButton>
            {/* to = '/' */}
          </Form>
        </FormContent>
      </FormWrap>
    </Container>


    </>
  );
}
export default Registration;
