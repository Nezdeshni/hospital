import React from "react";
import axios from "axios";

export default class Transtoday extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
        orders:[],
        isLoading: false,
        isError: false
      }
    }
async componentDidMount() {
  this.setState({isLoading: true})
    const response = await fetch("http://localhost:1300/medicines/stock/naorders",{method: "POST"});
    if(response.ok)
    {
        const orders = await response.json()

        console.log(orders)
        this.setState({orders, isLoading:false})
        var _state=this.state;
    }
    else
    {
        this.setState({isError:true, isLoading:false})
    }
}
/*
     axios.post(' http://localhost:1300/medicines/stock/naorders', {withCredentials: true}).then(res => {
         if(res)
         { //_state.orders=res.data;
             const orders = res.data
             console.log(orders)
             _sstate({orders, isLoading:false})
         }
         else
         {
             _sstate({isError:true, isLoading:false})
         }
    })
  }
*/
    renderTableRows = (n) => {
        let order = this.state.orders[n];
        return Object.values(JSON.parse(order.printform_data)).map(
                e => {
                    return (<tr>
                        <td>{e.n}</td>
                        <td>{e.cpi}</td>
                        <td>{e.q}</td>
                        <td>{e.cpi}*{e.q}
                        </td>
                    </tr>)
                })
    }

    approveOrder=async (n)=>{
       let nn=this.state.orders[n].sqn
        const response = await axios.post("http://localhost:1300/medicines/stock/orderapprove",{orn:nn});
        if(response)
        {
            window.location.reload()
        }
        else
        {
            window.location.reload()
        }

    }

  renderTable = (n) => {
      var _state= this.state;
      var _self=this;
      var order=this.state.orders[n]
      var pfd=JSON.parse(order.printform_data)
      let th="<th>препарат</th><th>цена</th><th>количество</th><th>стоимость</th>"
      var ref="/invoices/invoices_"+order.sqn.toString().padStart(8,"0")+"_.xlsx"
      return (
          <div style={{display:"inline-table",width:"100%"}}>
        <table style={{border: "3px solid black",padding: "20px 16px", width:"100%"}}>
            <caption style={{border: "3px solid black",padding: "20px 16px", "font-size":"x-small"}}><h1><b><p>Заказ из:</p> {order.lname}</b></h1>адрес:{order.fname}</caption>
            <thead style={{backgroundColor: "#FF416C", border: "1px solid black", color: "white"}}>
                <tr style={{border: "1px solid black", padding: "10px 8px"}}><th>препарат</th><th>цена</th><th>количество</th><th>стоимость</th></tr>
            </thead>
            <tbody style={{backgroundColor: "#ffdde1", border: "1px solid blue", textAlign: "center", padding: "10px 8px"}}>
                {this.renderTableRows(n)}
                <tr><td colSpan={4}>________________________________________________________________</td></tr>
            <tr><td colSpan={2}></td><td colSpan={2}>ИТОГО: {order.sub_totall}</td></tr>
                <tr><td colSpan={2}></td></tr><tr><td colSpan={2}><a href={ref.toString()}>Скачать накладную</a></td></tr>

            </tbody>
        </table>

              <div style={{display:"table-row", border: "3px solid black",padding: "20px 16px",}}>
                  <button
                      style={{width:"-webkit-fill-available","padding":"5px", margin: "5px", color: "black", "background-color": "darkgray"}}
                        onClick={()=>{_self.approveOrder(n)}}
                  >ПОДТВЕРДИТЬ</button>
              </div>
              <div style={{display:"inline-block",padding: "20px 16px"}}></div>
          </div>
      )
  }


  render() {
    const { orders, isLoading, isError } = this.state

    if (isLoading) {
      return <div>Загрузка...</div>
    }

    if (isError) {
      return <div>Ошибка</div>
    }

    return orders.length > 0
      ? (
        <div style={{display: 'grid', justifyContent: 'center'}}>
            {this.state.orders.map((e,i)=>this.renderTable(i))}
        </div>
      ) : (
        <div>
            нет новых заказов.
      </div>
      )
  }
}

