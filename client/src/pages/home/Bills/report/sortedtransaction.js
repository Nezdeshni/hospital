import React from "react";

export default class Sorttrans extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
        transactions:[],
        isLoading: false,
        isError: false
      }
    }
async componentDidMount() {
  this.setState({isLoading: true})

  const response = await fetch("http://localhost:1300/transaction/date")

  if(response.ok)
  {
      const transactions = await response.json()
      console.log(transactions)
      this.setState({transactions, isLoading:false})
    }
    else
    {
      this.setState({isError:true, isLoading:false})
    }
  }

  renderTableRows = () => {
    return this.state.transactions.map(transaction => {
        var ref="/invoices/invoices_"+transaction.billno.toString().padStart(8,"0")+"_.xlsx"
        return (
        <tr>
            <td>{transaction.billno}</td>
            <td><a href={ref.toString()}>Скачать накладную</a></td>
          <td>{transaction.fname}</td>
            <td>{transaction.lname}</td>
          <td>{transaction.billdate.split("T")[0]}</td>
          <td>{transaction.totalcost}</td>
        </tr>
      )
    })
  }
renderTableHeader = () => {
    return ["НОМЕР СЧЁТА","НАКАДНАЯ","АДРЕС","ОРГАНИЗАЦМЯ","ДАТА","СУММА"].map(attr => <th key={attr}>{attr.toUpperCase()}</th>)
  }


  render() {
    const { transactions, isLoading, isError } = this.state

    if (isLoading) {
      return <div>Загрузка...</div>
    }

    if (isError) {
      return <div>Ошибка</div>
    }

    return transactions.length > 0
      ? (
        <div style={{display: 'flex', justifyContent: 'center'}}>
        <table style={{border: "3px solid black",padding: "20px 16px", width: "950px"}}>
          <caption style={{border: "3px solid black",padding: "20px 16px"}}><h1><b>Транзакции отсортированные по дате</b></h1>
          Total records : {transactions.length}</caption>
          <thead style={{backgroundColor: "#FF416C", border: "1px solid black", color: "white"}}>
            <tr style={{border: "1px solid black", padding: "10px 8px"}}>
              {this.renderTableHeader()}
            </tr>
          </thead>
          <tbody style={{backgroundColor: "#ffdde1", border: "1px solid blue", textAlign: "center", padding: "10px 8px"}}>
            {this.renderTableRows()}
          </tbody>
        </table>
        </div>
      ) : (
        <div>
          No users.
      </div>
      )
  }
}

