import {useState} from 'react';
import {FormWrap, FormH1delete, FormContent, Form, FormInput, FormLabel, FormButton} from '../customerStyle'
import axios from 'axios'

const Searchbill = () =>{
    const [billno, setbillno] = useState(0);
    //console.log(sr_no);

    const lookBill = ()=>{
    axios.post('http://localhost:1300/transaction/seachBill', {
                billno: billno, withCredentials: true
    })
      .then(res => {
        console.log(res);
              if(res.data){
                var C_ID= res.data.C_ID;
                var billdate= res.data.billdate;
                var billno= res.data.billno;
                var totalcost= res.data.totalcost;
                var medname = res.data.med_name;
                var medcost = res.data.med_cost;
                var qty = res.data.quantity;
                var srno = res.data.sr_no;
                console.log("success", res);
                alert(`1. ID клиента:${C_ID}\n 2. Расчетная дата:${billdate}\n 3. Номер счёта${billno}\n 4. общая стоимость:${totalcost}\n 5.Название лекарства: ${medname}\n 6. Стоимость лекарств.: ${medcost}\n 7.Количество: ${qty}\n 8.серийный номер: ${srno}\n`)
              }
              else
              {
                alert("Bill not found.")
              }
    })
      .catch(error => {
                console.log("we have an error in catch",error);
    })
  }



  return (
    <>
    <FormWrap>
       <FormContent>
         <Form>
           <FormH1delete>Найти счёт в базе</FormH1delete>
           <FormLabel>Номер счёта</FormLabel>
             <FormInput type = 'number' required onChange ={(event) => {setbillno(event.target.value)}}/>
           <FormButton onClick = {lookBill} >Искать</FormButton>
         </Form>
       </FormContent>
     </FormWrap>
   </>
  )
}

export default Searchbill;
