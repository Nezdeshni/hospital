import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import Title from './../Title';
import {TR, TD, BUTTON} from './../customer/report/tableStyle'
// Generate Order Data


const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function Billdata() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Title>Данные счетов</Title>
      <Table>
        <TR><TD style ={{width:"70%"}}>Переводы упорядоченные по дате платежа</TD><TD><BUTTON to = '/sorttransaction'>Продолжить</BUTTON></TD></TR>
        <TR><TD style ={{width:"70%"}}>Переводы упорядоченные по стоимости</TD><TD><BUTTON to = '/transactiontotal'>Продолжить</BUTTON></TD></TR>
        <TR><TD style ={{width:"70%"}}>Переводы за сегодня</TD><TD><BUTTON to = '/transactiontoday'>Продолжить</BUTTON></TD></TR>
      </Table>
      <div className={classes.seeMore}>
      </div>
    </React.Fragment>

  );
}

