import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';


export default function Title(props) {
    var ses=JSON.parse(localStorage.getItem("session"));console.log(ses);
    if(ses.data.role=="customer"){window.location.replace("/order")};
    if(ses==null){window.location.replace("/")}


  return (
    <Typography component="h2" variant="h6" color="primary" gutterBottom>
      {props.children}
    </Typography>
  );
}

Title.propTypes = {
  children: PropTypes.node,
};
