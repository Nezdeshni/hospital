import {useState} from 'react';
import {FrmWrap, FrmContent, Frm, FrmInput, FrmH1,FrmButton, ExfrmInput} from '../customerStyle'
import axios from 'axios'

const Updatecustomer = () => {
    const [sr_no, setSr_no] = useState("");
    const [med_name, setMed_name] = useState("");
    const [qty_left, setQty_left] = useState("");
    const [med_cost ,setMed_cost] = useState("");
    const [exp_date,setExp_date] = useState("");
    const [med_mfg, setMed_mfg] = useState("");
    const [rac_loc, setRac_loc] = useState("");
    const [mfg_date, setMfg_date] = useState("");

  const updateMed = () =>{
    axios.put('http://localhost:1300/medicines/stock/update', {
                sr_no: sr_no,
                med_name: med_name,
                qty_left : qty_left,
                med_cost:med_cost,
                exp_date:exp_date,
                med_mfg:med_mfg,
                rac_loc:rac_loc,
                mfg_date: mfg_date, withCredentials: true
    })
      .then(data=>{
        console.log(data);
        if(!data)
            alert("Лекарство не найдено");
        else{
        console.log("success");
        alert("Лекарства успешно обновлены!");
        }
      })
              .catch(error => {
              console.log("we have an error in catch",error);
              alert("Предоставленные недопустимые данные")
    })
  }

  return (
    <>
     <FrmWrap>
        <FrmContent>
          <Frm onsubmit = "redirectLog(e)">
            <FrmH1>Обновить сведения</FrmH1>
            <FrmInput type = 'number' required placeholder = "Серийный номер" onChange ={(event) => {setSr_no(event.target.value)}}/>
              <FrmInput type = 'text' placeholder = "Название лекарства" required onChange ={(event) => {setMed_name(event.target.value)}}/>
              <FrmInput type = 'number' required placeholder = "Количество" onChange ={(event) => {setQty_left(event.target.value)}}/>
              <FrmInput type = 'number' required placeholder = "Стоимость" onChange ={(event) => {setMed_cost(event.target.value)}}/>
              <FrmInput type = 'date' required placeholder = "Дата изготовления" onChange ={(event) => {setMfg_date(event.target.value)}}/>
              <FrmInput type = 'date' required placeholder = "Срок годности" onChange ={(event) => {setExp_date(event.target.value)}}/>
              <FrmInput type = 'text' required placeholder = "Расположение стойки" onChange ={(event) => {setRac_loc(event.target.value)}}/>
              <FrmInput type = 'text' required placeholder = "Производитель" onChange ={(event) => {setMed_mfg(event.target.value)}}/>
            <FrmButton onClick = {updateMed}>Обновить!</FrmButton>
          </Frm>
        </FrmContent>
      </FrmWrap>
    </>
  );
}
export default Updatecustomer;
