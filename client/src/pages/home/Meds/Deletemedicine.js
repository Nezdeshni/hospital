import {useState} from 'react';
import {FormWrap, FormH1del, FormContent, Form, FormInput, FormH1, FormLabel, FormButton} from '../customerStyle'
import axios from 'axios'
import Divider from '@material-ui/core/Divider';

const Deletemedicine = () =>{
    const [sr_no, setSr_no] = useState(0);
    console.log(sr_no);

    const deleteMed = ()=>{
    axios.post('http://localhost:1300/medicines/stock/delete', { withCredentials: true,
                sr_no: sr_no,
    })
      .then(res => {
              if(res){
                console.log("success")
                alert("Успешно удален!")
              }
              else
              {
                alert("Препарат не найден не найден.")
              }
    })
      .catch(error => {
                console.log("we have an error in catch",error);
                alert("Препарат не удален.")
    })
  }


  return (
    <>
    <FormWrap>
       <FormContent>
         <Form>
           <FormH1del>Снять препарат с продажи</FormH1del>
           <FormLabel>Артикул препарата</FormLabel>
             <FormInput type = 'number' required onChange ={(event) => {setSr_no(event.target.value)}}/>
           <FormButton onClick = {deleteMed} >Удалить</FormButton>
         </Form>
       </FormContent>
     </FormWrap>
   </>
  )
}

export default Deletemedicine;
