import {useState} from 'react';
import {FrmWrap, FrmContent, Frm, FrmInput, FrmH1,FrmButton, ExfrmInput} from './../customerStyle'
import axios from 'axios'

const Updatecustomer = () => {
  const [fname, setFirstname] = useState("");
  const [lname, setLastname] = useState("");
  const [role, setRole] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");


      const updateCustomer = () =>{
          axios.post('http://localhost:1300/adminUpdate/adminUpdate', {
              username: username,
              password: password, withCredentials: true
          })
              .then(res => {
                  axios.post('http://localhost:1300/adminUpdate/adminReset', {
                      cid:res.data[0].cid,
                      fname:fname||res.data[0].fname,
                      lname:lname||res.data[0].lname,
                      role: role||res.data[0].role,
                      email:email||res.data[0].email,
                      username: username||res.data[0].username,
                      npassword: password||res.data[0].password,
                      withCredentials: true
              })
                        .then(res => {
                            if(res.data.reset===1){
                              alert("Сведения успешно обновлены")
                        }
                          else
                            {
                              alert("Запрос отклонен.")
                            }
                      })
                        .catch(error => {
                            console.log("we have an error in catch",error);
                      })})}




  return (
    <>
     <FrmWrap>
        <FrmContent>
          <Frm onsubmit = "redirectLog(e)">
            <FrmH1>Обновить сведения</FrmH1>
              <ExfrmInput type = 'text' required placeholder ="Аккаунт" onChange ={(event) => {setUsername(event.target.value)}}/>
              <ExfrmInput type = 'password' required placeholder ="Новый пароль" onChange ={(event) => {setPassword(event.target.value)}}/>
              <FrmInput type = 'text' required placeholder ="Адрес" onChange ={(event) => {setFirstname(event.target.value)}}/>
              <FrmInput type = 'text' required placeholder ="Название" onChange ={(event) => {setLastname(event.target.value)}}/>
              <FrmInput type = 'text' required placeholder ="Тип" onChange ={(event) => {setRole(event.target.value)}}/>
              <FrmInput type = 'email' required placeholder ="Email" onChange ={(event) => {setEmail(event.target.value)}}/>
            <FrmButton onClick = {updateCustomer}>Продолжить</FrmButton>
          </Frm>
        </FrmContent>
      </FrmWrap>
    </>
  );
}
export default Updatecustomer;
