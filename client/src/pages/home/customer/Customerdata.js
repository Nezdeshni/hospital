import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import Title from './../Title';
import {TR, TD, BUTTON} from './report/tableStyle'
// Generate Order Data


const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function Customerdata() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Title>Данные организаций</Title>
      <Table>
        <TR><TD style ={{width:"70%"}}>Последние зарегистрированные</TD><TD><BUTTON to = '/recentcustomers'>Продолжить</BUTTON></TD></TR>
      </Table>
      <div className={classes.seeMore}>
        <Link color="primary" href ='/'onClick={(e) => {
      e.preventDefault();
      window.location.href='/customerdata';
      }}>
          Весь список
        </Link>
      </div>
    </React.Fragment>

  );
}

