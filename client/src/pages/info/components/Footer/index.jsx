import React from 'react'
import { FooterContainer, FooterWrap, FooterLinkWrapper, FooterLinkItems, FooterLinkContainer, FooterLinkTitle, FooterLink, SocialMediaWrap, WebsiteRights } from './FooterElements'

const Footer = () => {
  return (
    <FooterContainer>
      <FooterWrap>

          <SocialMediaWrap>
            <WebsiteRights>создан в 2024 году</WebsiteRights>
          </SocialMediaWrap>
      </FooterWrap>
    </FooterContainer>
  )
}

export default Footer
