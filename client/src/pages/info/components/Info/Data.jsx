import dashboard from "./../../images/svg-6.svg";

export const homeObjOne = {
  id: 'about',
  lightBg: true,
  lightText: false,
  lighttextDesc: false,
  topLine: 'Легко управлять записями',
  headline: 'Неограниченное ведение документации с нулевой дополнительной платой',
  description: 'Получите доступ к нашему эксклюзивному веб-приложению, которое позволяет управлять своими фирмами, с которых взимается дополнительная плата.',
  buttonLabel: 'приступить к работе',
  imgStart: false,
  img: dashboard,
  alt: 'car',
  dark: true,
  primary: true,
  darkText: false
};

export const homeObjTwo = {
  id: 'discover',
  lightBg: true,
  lightText: false,
  lighttextDesc: false,
  topLine: 'Неограниченный доступ',
  headline: 'Войти в свой аккаунт в любое время',
  description: 'Получить доступ к нашему эксклюзивному приложению, которое позволяет отправлять неограниченные транзакции, в которых взимается любая плата.',
  buttonLabel: 'узнайте больше',
  imgStart: true,
  img: '/images/svg-4.svg',
  alt: 'Piggybanck',
  dark: false,
  primary: false,
  darkText: true
};
