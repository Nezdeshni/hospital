import React, {useState} from 'react'
import { HeroContainer,  HeroContent, HeroH1, HeroP, HeroBtnWrapper, ArrowForward, ArrowRight } from './HeroElements'
import { Button } from '../ButtonElements'

const Hero = () => {
  const [hover, setHover] = useState(false);

  const onHover = () => {
    setHover(!hover)
  }

  return (
    <HeroContainer id='home'>

      <HeroContent>
        <HeroH1> СУЗ Медизделия</HeroH1>
        <HeroP>Обеспечивает надлежащее внимание и защиту всех записей, а также более эффективному и действенному извлечению содержащихся в них метаданных и информации.</HeroP>
        <HeroBtnWrapper>
          <Button to='/signin' onMouseEnter={onHover} onMouseLeave={onHover} primary='true' dark='true'               smooth={true} duration={500} spy={true} exact='true' offset={-80}>
              приступить {hover ? <ArrowForward /> : <ArrowRight/>}
          </Button>
        </HeroBtnWrapper>
      </HeroContent>
    </HeroContainer>
  )
}

export default Hero
