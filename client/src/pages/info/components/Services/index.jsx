import React from 'react'
import Icon1 from '../../images/svg-8.svg'
import Icon2 from '../../images/svg-7.svg'
import Icon3 from '../../images/svg-9.svg'
import {ServicesContainer, ServicesH1, ServicesWrapper, ServicesCard, ServicesIcon, ServicesH2, ServicesP} from './ServiceElements'

const Services = () => {
  return (
    <ServicesContainer id='services'>
      <ServicesH1>наши услуги</ServicesH1>
      <ServicesWrapper>
        <ServicesCard>
          <ServicesIcon src={Icon1}/>
          <ServicesH2>Управлять информацией о клиенте</ServicesH2>
          <ServicesP>Мы помогаем вам создавать, удалять, обновлять и искать информацию о клиенте.</ServicesP>
        </ServicesCard>
        <ServicesCard>
        <ServicesIcon src={Icon2}/>
          <ServicesH2>Управлять реквизитами операций</ServicesH2>
          <ServicesP>Мы помогаем вам создавать, удалять, обновлять и искать транзакции, а также создавать отчеты.</ServicesP>
        </ServicesCard>
        <ServicesCard>
          <ServicesIcon src={Icon3}/>
          <ServicesH2>Вести медицинские записи</ServicesH2>
          <ServicesP>Добавить новые лекарства, обновить старые и удалить все просроченные или из запаса лекарств.</ServicesP>
        </ServicesCard>
      </ServicesWrapper>
    </ServicesContainer>
  )
}

export default Services
