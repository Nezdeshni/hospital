import {useState} from 'react';
import { FrmWrap, FrmContent, Formadd, FormInputadd, FormH1add, FormButtonadd} from './../customerStyle'

const Addcustomer = () => {
    const [fname, setFirstname] = useState("");
    const [lname, setLastname] = useState("");
    const [role, setRole] = useState("");
    const [email, setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");


    const addUser = () =>{
        console.log(username);
        fetch("http://localhost:1300/register/create",{
            method:"post",
            headers:{
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                fname: fname,
                lname:lname,
                role:role,
                email:email,
                username: username,
                password: password,
            })
        })
            // .then(res=> res.json())
            .then(data=>{
                console.log(data);
                console.log("success");
                alert("Registration successful!");
            })
            .catch(err=>{
                console.log("we have an error in catch",err);
            })
    }

    return (
        <>
            <FrmWrap>
                <FrmContent>
                    <Formadd onsubmit = "redirectLog(e)">
                        <FormH1add>Добавлять новых клиентов</FormH1add>
                        <FormInputadd type = 'hidden' placeholder = "Имя"  onChange ={(event) => {setFirstname(event.target.value)}}/>
                        <FormInputadd type = 'text' required placeholder = "Название организации" onChange ={(event) => {setLastname(event.target.value)}}/>
                        <FormInputadd type = 'text' required placeholder = "Тип" onChange ={(event) => {setRole(event.target.value)}}/>
                        <FormInputadd type = 'email' required placeholder = "Email организации" onChange ={(event) => {setEmail(event.target.value)}}/>
                        <FormInputadd type = 'text' required placeholder = "Учётная запись (латинскими буквами)" onChange ={(event) => {setUsername(event.target.value)}}/>
                        <FormInputadd type = 'password' required placeholder = "Пароль" onChange ={(event) => {setPassword(event.target.value)}}/>
                        <FormButtonadd onClick = {addUser}>Продолжить</FormButtonadd>
                    </Formadd>
                </FrmContent>
            </FrmWrap>
        </>
    );
}
export default Addcustomer;
