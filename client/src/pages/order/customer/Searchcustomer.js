import {useState} from 'react';
import {FormWrap, FormH1del, FormContent, Form, FormInput, FormH1, FormLabel, FormButton} from './../customerStyle'
import axios from 'axios'
import Divider from '@material-ui/core/Divider';

const Searchcustomer = () => {
  const [username, setUsername] = useState("");
  var fname = "";
  var lname = "";
  var cid = "";
  var pincode = "";
  var email = "";
  var role="";
  const Searchcustomer = () =>{
    console.log(username);
    axios.post('http://localhost:1300/searchcustomer/adminsearch', {
      username: username, withCredentials: true
    })
      .then(res => {
              console.log(res);
              if(res.data){
                console.log("success")
                fname= res.data[0].fname;
                lname= res.data[0].lname;
                role=  res.data[0].role;
                email= res.data[0].email;
                pincode= res.data[0].pincode;
                alert(`User found! \n1. Account=${fname} Организавция=${lname}\n2. Тип=${role}\n3. Email=${email}\n4. Pincode=${pincode}`);
              }
              else
              {
                alert("Request denied.")
              }
    })
      .catch(error => {
                console.log("we have an error in catch",error);
              alert("Invalid ID")
    })
  }

  const Deletecustomer = () =>{
    console.log(username);
    axios.post('http://localhost:1300/searchcustomer/admindelete', {
                username: username, withCredentials: true
    })
      .then(res => {
              if(res.data.login===1){
                console.log("success")
                alert("Account deleted successfully!")
              }
              else
              {
                alert("Account deleted.")
              }
    })
      .catch(error => {
                console.log("we have an error in catch",error);
                alert("Account deleted.")
    })
  }
  return (
    <>
    <FormWrap>
       <FormContent>
         <Form>
           <FormH1>Искать пользователя</FormH1>
           <FormLabel>Имя пользователя</FormLabel>
             <FormInput type = 'text' required onChange ={(event) => {setUsername(event.target.value)}}/>
           <FormButton onClick = {Searchcustomer} >Искать</FormButton>
           <Divider/>
           <FormH1del>Удалить пользователя</FormH1del>
           <FormLabel>Имя пользователя</FormLabel>
             <FormInput type = 'text' required onChange ={(event) => {setUsername(event.target.value)}}/>
             <FormLabel>Причина</FormLabel>
             <FormInput type = 'text' required/>
           <FormButton onClick = {Deletecustomer} >Удалить</FormButton>
         </Form>
       </FormContent>
     </FormWrap>
   </>
  )
}
export default Searchcustomer;

