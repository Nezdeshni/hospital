import {useState} from 'react';
import {FrmWrap, FrmContent, Frm, FrmInput, FrmH1,FrmButton, ExfrmInput} from './../customerStyle'
import axios from 'axios'

const Updatecustomer = () => {
  const [fname, setFirstname] = useState("");
  const [lname, setLastname] = useState("");
  const [role, setRole] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [npassword, setNPassword] = useState("");

  const updateCustomer = () =>{
    axios.post('http://localhost:1300/adminUpdate/userVerifyed', {
                username: username,
                password: password, withCredentials: true
    })
      .then(res => {

console.log("*****************")
console.log(res)
console.log("*******************")
                    axios.post('http://localhost:1300/adminUpdate/userReset', {
                        cid:res.data[0].cid,
                        fname:fname||res.data[0].fname,
                        lname:lname||res.data[0].lname,
                        role: role||res.data[0].role,
                        email:email||res.data[0].email,
                        username: username||res.data[0].username,
                        npassword: npassword||res.data[0].password,
                        withCredentials: true
                        })
                        .then(res => {
                            if(res.data.reset===1){
                              alert("Сведения успешно обновлены")
                        }
                          else
                            {
                              alert("Запрос отклонен.")
                            }
                      })
                        .catch(error => {
                            console.log("we have an error in catch",error);
                      })

  })

  }
   var dat= JSON.parse(localStorage.session).data
  return (
    <>
     <FrmWrap>
        <FrmContent>
          <Frm onsubmit = "redirectLog(e)">
            <FrmH1>Обновить сведения</FrmH1>
              <ExfrmInput type = 'text' required placeholder ="Account" onChange ={(event) => {setUsername(event.target.value)}}/>
              <ExfrmInput type = 'password' required placeholder ="Текущий пароль"  onChange ={(event) => {setPassword(event.target.value)}}/>
              <FrmInput type = 'password' required placeholder ="Новый пароль" onChange ={(event) => {setNPassword(event.target.value)}}/>
              <FrmInput type = 'text' required placeholder ="Адрес" onChange ={(event) => {setFirstname(event.target.value)}}/>
              <FrmInput type = 'text' required placeholder ="Организация"  onChange ={(event) => {setLastname(event.target.value)}}/>
              <FrmInput type = 'email' required placeholder ="Email" onChange ={(event) => {setEmail(event.target.value)}}/>
            <FrmButton onClick = {updateCustomer}>Продолжить</FrmButton>
          </Frm>
        </FrmContent>
      </FrmWrap>
    </>
  );
}
export default Updatecustomer;
