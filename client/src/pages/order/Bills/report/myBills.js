import React from "react";
import axios from "axios";
export default class PayBills extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
        orders:[],
        isLoading: false,
        isError: false
      }
    }
async componentDidMount() {
    let ses=JSON.parse(localStorage.getItem("session"))
    this.setState({isLoading: true})
    const response = await axios.post("http://localhost:1300/medicines/stock/myorders",{me:ses.data.cid});
    console.log(response)
    if(response.status==200)
    {
        response.data.forEach(e=>e.printform_data=JSON.parse(e.printform_data))
        const orders = response.data.sort((a,b)=>{
            if(a.order_state[0]<b.order_state[0]){
                return -1
            }
            return 1
        })

        console.log(orders)
        this.setState({orders, isLoading:false})
    }
    else
    {
        this.setState({isError:true, isLoading:false})
    }
  }
    renderTableRows = (n) => {
        let order = this.state.orders[n];
        return Object.values(order.printform_data).map(
            e => {
                return (<tr>
                    <td>{e.n}</td>
                    <td>{e.cpi}</td>
                    <td>{e.q}</td>
                    <td>{e.cpi}*{e.q}
                    </td>
                </tr>)
            })
    }
    renderTable = (n) => {
        var _state= this.state;
        var _self=this;
        var order=this.state.orders[n]
        var pfd=order.printform_data
        let th="<th>препарат</th><th>цена</th><th>количество</th><th>стоимость</th>"
        var ref="/invoices/invoices_"+order.sqn.toString().padStart(8,"0")+"_.xlsx"
        return (
            <div style={{display:"inline-table",width:"100%"}}>
                <table style={{border: "3px solid black",padding: "20px 16px", width:"100%"}}>
                    <caption style={{border: "3px solid black",padding: "20px 16px", "font-size":"x-small"}}><h1><b><p>Заказ из:</p> {order.lname}</b></h1>адрес:{order.fname}</caption>
                    <thead style={{backgroundColor: "#FF416C", border: "1px solid black", color: "white"}}>
                    <tr style={{border: "1px solid black", padding: "10px 8px"}}><th>препарат</th><th>цена</th><th>количество</th><th>стоимость</th></tr>
                    </thead>
                    <tbody style={{backgroundColor: "#ffdde1", border: "1px solid blue", textAlign: "center", padding: "10px 8px"}}>
                    {this.renderTableRows(n)}
                    <tr><td colSpan={4}>________________________________________________________________</td></tr>
                    <tr><td colSpan={2}></td><td colSpan={2}>ИТОГО: {order.sub_totall}</td></tr>
                    <tr><td colSpan={2}></td><td colSpan={2}><a href={ref.toString()}>Скачать накладную</a></td></tr>
                    </tbody>
                </table>

                <div style={{display:"table-row", border: "3px solid black",padding: "20px 16px",}}>
                    {order.order_state==="created"?(
                        <button
                            style={{width:"-webkit-fill-available","padding":"5px", margin: "5px", color: "black", "background-color": "red"}}
                            onClick={async ()=>{await _self.revokeOrder(this.state.orders[n].sqn)}}
                        >ОТМЕНИТЬ</button>
                    ):(
                        <span style={{display:"block",width:"100%","padding":"5px", margin: "5px", color: "#3349ac", "background-color": "#39da66"}}>Заказ подтверждён</span>
                    )}

                </div>
                <div style={{display:"inline-block",padding: "20px 16px"}}></div>
            </div>
        )
    }


    render() {
        const { orders, isLoading, isError } = this.state

        if (isLoading) {
            return <div>Загрузка...</div>
        }

        if (isError) {
            return <div>Ошибка</div>
        }

        return orders.length > 0
            ? (
                <div style={{display: 'grid', justifyContent: 'center'}}>
                    {this.state.orders.map((e,i)=>this.renderTable(i))}
                </div>
            ) : (
                <div>
                    нет новых заказов.
                </div>
            )
    }

   async  revokeOrder(n) {
        const response = await axios.post("http://localhost:1300/medicines/stock/orderrevoke",{orn:n});
        window.location.reload()
    }
}
