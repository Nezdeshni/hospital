import React, {useState} from "react";
import {FormButton, FormInput} from "../../customerStyle";
import axios from "axios";
import {FormBadd} from "../../../home/customerStyle";
const setMins=()=>{

}
const fillOrder=(s,n,id,v)=>{
    var bb=document.createElement("button");
    bb.onclick=(event)=>{event.preventDefault(); do_order(s)}
    bb.innerHTML="ЗАКАЗАТЬ";
    bb.style=`width:-webkit-fill-available;padding:5px; margin: 5px; color: black; background-color: darkgray`
    var sm=s.meds.filter(e=>e.sr_no==n)[0];
    var ql=sm.qty_left;
    if(ql<=0){
         return ql
     }

    s.order[n]={
        q:sm.qty_left-v>=0?v:sm.qty_left,
        n:sm.med_name,
        cpi:sm.med_cost,
        mfr:sm.med_mfg,
        sr_no:sm.sr_no
    }
    let subtotal=Object.values(s.order).reduce((p,c)=>{return p+c.cpi*c.q},0)

    let tb=Object.keys(s.order).map(e=>`<tr><td>${s.order[e].n}</td><td>${s.order[e].q}x${s.order[e].cpi}</td></tr>`);
    //v+=<tr><td><FormButton onClick ={delBill}>Заказать</FormButton>
        let a=Array.from(document.getElementsByTagName("TBODY"))[1]
        //let f=a.lastChild.outerHTML;
        a.innerHTML=tb.join('')+"<tr><td>итого:</td><td>"+subtotal+"</td></tr>";
        a.appendChild(bb);
        let ids=Object.keys(s.order);
        let b=Array.from(document.getElementsByTagName("TABLE"))[0]
        b.childNodes[2].childNodes.forEach((e,i)=>{
            let k=e.children[0].innerHTML;
            if(ids.includes(k)){
                e.children[2].innerHTML=s.meds[i].qty_left-s.order[k].q
            }
        })
    return v;

}

const do_order = (s)=>{
    console.log(s)
    let um=Object.keys(s.order).map(e=>[s.order[e].sr_no,s.meds.filter(el=>el.sr_no==s.order[e].sr_no)[0].qty_left-s.order[e].q])
    s.order={ord:s.order,um:um,ses:JSON.parse(localStorage.getItem('session'))}
    axios.post('http://localhost:1300/medicines/stock/insertorder', {
        fd:JSON.stringify(s.order),withCredentials: true
    }).then(res => {
        if(res){
            alert("Заказ оформлен.")
            window.location.reload()

        }
        else
        {
            alert("Bill not found.")
        }
    })
    /*
    axios.post('http://localhost:3000/transaction/delete', {
        billno: billno
    })
        .then(res => {
            if(res){
                console.log("success")
                alert("Bill deleted successfully from Transaction table!")
            }
            else
            {
                alert("Bill not found.")
            }
        })
        .catch(error => {
            console.log(billno);
            console.log("we have an error in catch",error);
            alert("Account not deleted.")
        })
        */

}

export default class GetMedicine extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            meds:[],
            isLoading: false,
            isError: false,
            do_order: do_order,
            order:{},
            fillOrder:fillOrder,
        }
    }
    async componentDidMount() {
        this.setState({isLoading: true})

        const response = await fetch("http://localhost:1300/medicines/stock/expirySort")

        if(response.ok)
        {
            const meds = await response.json()

            console.log(meds)
            this.setState({meds, isLoading:false})
            var _state=this.state;

        }
        else
        {
            this.setState({isError:true, isLoading:false})
        }
    }

    renderTableRows = () => {
        console.log(this)
        var _state= this.state;
        return this.state.meds.map(meds => {
            return (
                <tr key={meds.sr_no}>
                    <td>{meds.sr_no}</td>
                    <td>{meds.med_name}</td>
                    <td>{meds.qty_left}</td>
                    <td>{meds.med_cost} руб. 0 коп.</td>
                    <td>{(new Date(meds.exp_date)).toLocaleDateString()}</td>
                    <td>{meds.med_mfg}</td>
                    <td>{meds.rac_loc}</td>
                    <td>{(new Date(meds.mfg_date)).toLocaleDateString()}</td>
                    <td><FormInput min={0} max={meds.qty_left} style={{padding:"5px 16px","border-radius": "4px",width: "-webkit-fill-available",margin: "8px"}} type = 'number' required onChange ={(event) => {

                        let mn=event.target.parentElement.parentNode.cells[0].textContent;
                        if(fillOrder(_state,mn,event.target.parentElement.parentNode.cells[1].textContent,event.target.value)==0){
                            event.target.value=_state.order[mn].q
                        }
                        console.log(_state);
                        console.log(event.target)}}/></td>
                </tr>
            )
        })
    }
    renderTableHeader = () => {
        return Object.keys(this.state.meds[0]).map(attr => <th key={attr}>{attr.toUpperCase()}</th>).concat(<th>Заказать</th>)
      }

    render() {
        const { meds, isLoading, isError, do_order, order, fillOrder } = this.state;
        var _state=this.state;
        if (isLoading) {
            return <div>Загрузка...</div>
        }

        if (isError) {
            return <div>Ошибка</div>
        }

        return meds.length > 0
            ? (
                <div style={{display: 'flex', justifyContent: 'center'}}>
                    <table style={{width:"60%", border: "3px solid black",padding: "20px 16px"}}>
                        <caption style={{border: "3px solid black",padding: "20px 16px"}}><h1><b>Перйскурант</b></h1></caption>
                        <thead style={{backgroundColor: "#FF416C", border: "1px solid black", color: "white"}}>
                        <tr style={{border: "1px solid black", padding: "10px 8px"}}>
                            {this.renderTableHeader()}
                        </tr>
                        </thead>
                        <tbody style={{backgroundColor: "#ffdde1", border: "1px solid blue", textAlign: "center", padding: "10px 8px"}}>
                        {this.renderTableRows()}
                        </tbody>
                    </table>
                    <table style={{ width:"35%", border: "3px solid black",padding: "20px 16px"}}><caption style={{border: "3px solid black",padding: "20px 16px"}}><h1><b>Заказ</b></h1></caption>
                       <tbody> </tbody>
                    </table>
           </div>
            ) : (
                <div>
                    No meds.
                </div>
            )
    }
}

