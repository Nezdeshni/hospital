import {useState} from 'react';
import {FormWrap, FormH1del, FormContent, Form, FormInput, FormH1, FormLabel, FormButton} from '../customerStyle'
import axios from 'axios'
import Divider from '@material-ui/core/Divider';

const Deletemedicine = () =>{
    const [sr_no, setSr_no] = useState(0);
    console.log(sr_no);

    const deleteMed = ()=>{
    axios.post('http://localhost:1300/medicines/stock/delete', {
                sr_no: sr_no, withCredentials: true
    })
      .then(res => {
              if(res){
                console.log("success")
                alert("Успешно удаленный аккаунт!")
              }
              else
              {
                alert("Счет не найден.")
              }
    })
      .catch(error => {
                console.log("we have an error in catch",error);
                alert("Учетная запись не удалена.")
    })
  }


  return (
    <>
    <FormWrap>
       <FormContent>
         <Form>
           <FormH1del>Снять препарат с продажи</FormH1del>
           <FormLabel>Артикул препарата</FormLabel>
             <FormInput type = 'number' required onChange ={(event) => {setSr_no(event.target.value)}}/>
             <FormLabel>Причина</FormLabel>
             <FormInput type = 'text' required/>
           <FormButton onClick = {deleteMed} >Удалить</FormButton>
         </Form>
       </FormContent>
     </FormWrap>
   </>
  )
}

export default Deletemedicine;
